import { GoogleLoginComponent } from './google-login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, IonicModule],
    declarations: [GoogleLoginComponent],
    exports: [GoogleLoginComponent]
})
export class GoogleLoginComponentModule { }
