import { Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-google-login',
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.scss'],
})
export class GoogleLoginComponent implements OnInit {

  formRegistro: FormGroup;
  formLogin: FormGroup;
  mensaje = '';
  mensajeLogin = '';
  registro = true;
  login = false;
  loged = false;
  perfil = null;

  constructor(
    private firebaseAuthentication: FirebaseAuthentication,
    protected formBuilder: FormBuilder,
    private firebaseAnalytics: FirebaseAnalytics,
    public alertController: AlertController ) {

    this.firebaseAnalytics.setCurrentScreen('Registro')
      .then(function (res) {
        console.log(res);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  checkIniciarSesion(registro: boolean, login: boolean): void {
    this.registro = registro;
    this.login = login;
  }

  async presentAlert(titulo: string, subtitulo: string, mensaje: string, boton: string) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      buttons: [boton]
    });

    await alert.present();
  }

  processForm(): void {

    const valid = this.formRegistro.valid;
    if (valid) {

      if (this.formRegistro.controls.contrasena.value === this.formRegistro.controls.recontrasena.value) {

        const user = {
          nombre: this.formRegistro.controls.nombre.value,
          apellido: this.formRegistro.controls.apellido.value,
          genero: this.formRegistro.controls.genero.value,
          profesion: this.formRegistro.controls.profesion.value,
          id: this.formRegistro.controls.id.value,
          email: this.formRegistro.controls.email.value,
          celular: this.formRegistro.controls.celular.value,
          ciudad: this.formRegistro.controls.ciudad.value,
          departamento: this.formRegistro.controls.departamento.value,
          contrasena: this.formRegistro.controls.contrasena.value,
        };
        const registroFirebase = this.firebaseAnalytics;
        const contextoActual = this;
        // tslint:disable-next-line: max-line-length
        this.firebaseAuthentication.createUserWithEmailAndPassword(this.formRegistro.controls.email.value, this.formRegistro.controls.contrasena.value)
          .then(function(res) {
            console.log(res);
            // tslint:disable-next-line: no-conditional-assignment
            if (res === 'OK') {

              // tslint:disable-next-line: max-line-length
              contextoActual.presentAlert('¡Gracias ' + user.nombre, 'Tu registro se ha completado correctamente', '<small>Ahora ingresa con tu correo y contraseña</small>', 'Continuar');
              contextoActual.registro = false;
              contextoActual.login = true;
              contextoActual.perfil = user;

              registroFirebase.logEvent('sign_up', null)
                .then(function (res) {
                  console.log(res);
                })
                .catch(function (error) {
                  console.log(error);
                });

              registroFirebase.setCurrentScreen('Inicio de sesión')
                .then(function (res) {
                  console.log(res);
                })
                .catch(function (error) {
                  console.log(error);
                });

              registroFirebase.setUserProperty('genero', user.genero)
                .then(function (res) {
                  console.log(res);
                })
                .catch(function (error) {
                  console.log(error);
                });

              registroFirebase.setUserProperty('profesion', user.profesion)
                .then(function (res) {
                  console.log(res);
                })
                .catch(function (error) {
                  console.log(error);
                });

              contextoActual.formRegistro.controls.nombre.reset();
              contextoActual.formRegistro.controls.apellido.reset();
              contextoActual.formRegistro.controls.genero.reset();
              contextoActual.formRegistro.controls.profesion.reset();
              contextoActual.formRegistro.controls.id.reset();
              contextoActual.formRegistro.controls.email.reset();
              contextoActual.formRegistro.controls.celular.reset();
              contextoActual.formRegistro.controls.ciudad.reset();
              contextoActual.formRegistro.controls.departamento.reset();
              contextoActual.formRegistro.controls.contrasena.reset();
              contextoActual.formRegistro.controls.recontrasena.reset();
            }
          })
          .catch (function(error) {
            console.log(error);
            // tslint:disable-next-line: max-line-length
            contextoActual.presentAlert('Oops!', '', 'Parece que ya estas registrado, inténtalo con otro email o inicia sesión con tu correo y contraseña previamente registrados.', 'Volver');
          });
      } else {
        this.presentAlert('Oops!', '', 'Las contraseñas no coinciden.', 'Volver');
        this.formRegistro.controls.contrasena.reset();
        this.formRegistro.controls.recontrasena.reset();
      }
    } else {
      this.presentAlert('¡Espera!', '', 'Completa la información para finalizar el registro', 'Volver');
    }
  }

  processFormLogin(): void {

    const valid = this.formLogin.valid;
    if (valid) {
      const userLogin = {
        email: this.formLogin.controls.email.value,
        contrasena: this.formLogin.controls.contrasena.value,
      };

      const loginFirebase = this.firebaseAnalytics;
      const contextoActual = this;
      // tslint:disable-next-line: max-line-length
      this.firebaseAuthentication.signInWithEmailAndPassword(this.formLogin.controls.email.value, this.formLogin.controls.contrasena.value)
        .then(function(res) {
          console.log(res);

          contextoActual.login = false;
          contextoActual.loged = true;

          // tslint:disable-next-line: no-conditional-assignment
          if (res === 'OK') {
            loginFirebase.logEvent('login', userLogin)
              .then(function (res) {
                console.log(res);
              })
              .catch(function (error) {
                console.log(error);
              });

            loginFirebase.setUserId(userLogin.email)
              .then(function (res) {
                console.log(res);
              })
              .catch(function (error) {
                console.log(error);
              });

            loginFirebase.setCurrentScreen('Información Personal')
              .then(function (res) {
                console.log(res);
              })
              .catch(function (error) {
                console.log(error);
              });

            loginFirebase.logEvent('screen_view', { page: 'Tab Información Personal' })
              .then(function (res) {
                console.log(res);
              })
              .catch(function (error) {
                console.log(error);
              });

            contextoActual.formLogin.controls.email.reset();
            contextoActual.formLogin.controls.contrasena.reset();
          }
        })
        .catch(function(error) {
            console.log(error);
            contextoActual.presentAlert('Oops!', '', 'Ocurrió un error en el inicio de sesión, verifica tus datos.', 'Reintentar');
        });
    } else {
      this.presentAlert('¡Espera!', '', 'Ingresa un email y constraseña válidos', 'Volver');
    }

  }

  cerrarSesion(): void {

    const loginFirebase = this.firebaseAnalytics;
    const contextoActual = this;

    this.firebaseAuthentication.signOut()
      .then(function (res) {
        console.log(res);

        contextoActual.registro = true;
        contextoActual.loged = false;
        contextoActual.login = false;
        contextoActual.perfil = null;

        loginFirebase.logEvent('log_out', null)
          .then(function (res) {
            console.log(res);
          })
          .catch(function (error) {
            console.log(error);
          });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  ngOnInit() {
    this.formRegistro = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      apellido: new FormControl('', Validators.required),
      genero: new FormControl('', Validators.required),
      profesion: new FormControl('', Validators.required),
      id: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]),
      celular: new FormControl('', Validators.required),
      ciudad: new FormControl('', Validators.required),
      departamento: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required),
      recontrasena: new FormControl('', Validators.required)
    });


    this.formLogin = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]),
      contrasena: new FormControl('', Validators.required)
    });
  }

}
