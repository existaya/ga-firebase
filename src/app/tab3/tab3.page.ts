import { Component } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  formDeporte: FormGroup;

  constructor(
    protected formBuilder: FormBuilder,
    private firebaseAnalytics: FirebaseAnalytics,
    public alertController: AlertController) {

    this.firebaseAnalytics.logEvent('screen_view', { page: 'Tab Información deportiva' }).then(res => console.log(res));
    this.firebaseAnalytics.setCurrentScreen('Información Deportiva').then(res => console.log(res));

  }

  async presentAlert(titulo: string, subtitulo: string, mensaje: string, boton: string) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      buttons: [boton]
    });

    await alert.present();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.formDeporte = this.formBuilder.group({
      deporteFavorito: new FormControl('', Validators.required)
    });
  }

  processForm() {
    const valid = this.formDeporte.valid;
    if (valid) {
      const deporte = this.formDeporte.controls.deporteFavorito.value;

      this.presentAlert('Gracias!', '', 'Ya registraste tu deporte favorito.', 'Continuar');

      this.firebaseAnalytics.setUserProperty('deporte', deporte)
        .then(function (res) {
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
        });

      this.firebaseAnalytics.logEvent('deporte_favorito', { name: deporte})
        .then(function (res) {
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
        });

      this.formDeporte.controls.deporteFavorito.reset();

    } else {
      this.presentAlert('¡Espera!', '', 'Completa la información solicitada', 'Volver');
    }
  }

}
