import { Component } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  formMusica: FormGroup;

  constructor(
    protected formBuilder: FormBuilder,
    private firebaseAnalytics: FirebaseAnalytics,
    public alertController: AlertController) {

    this.firebaseAnalytics.logEvent('screen_view', { page: 'Tab Información Musical' }).then(res => console.log(res));
    this.firebaseAnalytics.setCurrentScreen('Información Musical').then(res => console.log(res));
  }

  async presentAlert(titulo: string, subtitulo: string, mensaje: string, boton: string) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      buttons: [boton]
    });

    await alert.present();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.formMusica = this.formBuilder.group({
      generoMusical: new FormControl('', Validators.required)
    });
  }

  processForm() {
    const valid = this.formMusica.valid;
    if (valid) {
      const genero = this.formMusica.controls.generoMusical.value;

      this.presentAlert('Gracias!', '', 'Ya registraste tus preferencias musicales.', 'Continuar');

      this.firebaseAnalytics.setUserProperty('musica', genero)
        .then(function (res) {
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
        });

      this.firebaseAnalytics.logEvent('genero_musical', { name: genero})
        .then(function (res) {
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
        });

      this.formMusica.controls.generoMusical.reset();

    } else {
      this.presentAlert('¡Espera!', '', 'Completa la información solicitada', 'Volver');
    }
  }

}
