import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Component implements OnInit {

  formAlimentacion: FormGroup;

  constructor(
    protected formBuilder: FormBuilder,
    private firebaseAnalytics: FirebaseAnalytics,
    public alertController: AlertController ) {

    this.firebaseAnalytics.logEvent('screen_view', { page: 'Tab Información Alimentación' })
      .then(function (res) {
        console.log('screen_view Tab Información Alimentación ' + res);
      })
      .catch(function (error) {
        console.log('screen_view Tab Información Alimentación ' + error);
      });

    this.firebaseAnalytics.setCurrentScreen('Información Alimentación')
      .then(function (res) {
        console.log('setCurrentScreen Información Alimentación ' + res);
      })
      .catch(function (error) {
        console.log('setCurrentScreen Información Alimentación ' + error);
      });

  }

  ngOnInit() {
    this.formAlimentacion = this.formBuilder.group({
      comidas: new FormControl('', Validators.required),
      bebidas: new FormControl('', Validators.required),
      postres: new FormControl('', Validators.required)
    });
  }

  async presentAlert(titulo: string, subtitulo: string, mensaje: string, boton: string) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      buttons: [boton]
    });

    await alert.present();
  }

  confirmarTutotial(): void {
    this.presentAlert('Yei!!', 'Gracias por completar la información', '', 'Finalizar');
    this.firebaseAnalytics.logEvent('tutorial_complete', null)
      .then(function (res) {
        console.log('logEvent tutorial_complete' + res);
      })
      .catch(function (error) {
        console.log('logEvent tutorial_complete' + error);
      });
  }

  processForm(): void {
    const valid = this.formAlimentacion.valid;
    if (valid) {
      const comidas = this.formAlimentacion.controls.comidas.value;
      const bebidas = this.formAlimentacion.controls.bebidas.value;
      const postres = this.formAlimentacion.controls.postres.value;

      this.presentAlert('Gracias!', '', 'Ya registraste tu alimentación.', 'Continuar');

      this.firebaseAnalytics.logEvent('preferencias_alimenticias', { comida: comidas, bebida: bebidas, postre: postres })
        .then(function (res) {
          console.log('logEvent preferencias_alimenticias ' + res);
        })
        .catch(function (error) {
          console.log('logEvent preferencias_alimenticias ' + error);
        });

      this.firebaseAnalytics.setUserProperty('comida', comidas)
        .then(function (res) {
          console.log('setUserProperty comida: ' + res);
        })
        .catch(function (error) {
          console.log('setUserProperty comida: ' + error);
        });

      this.firebaseAnalytics.setUserProperty('bebida', bebidas)
        .then(function (res) {
          console.log('setUserProperty bebida: ' + res);
        })
        .catch(function (error) {
          console.log('setUserProperty bebida: ' + error);
        });

      this.firebaseAnalytics.setUserProperty('postre', postres)
        .then(function (res) {
          console.log('setUserProperty postre: ' + res);
        })
        .catch(function (error) {
          console.log('setUserProperty postre: ' + error);
        });

      this.formAlimentacion.controls.comidas.reset();
      this.formAlimentacion.controls.bebidas.reset();
      this.formAlimentacion.controls.postres.reset();

    } else {
      this.presentAlert('¡Espera!', '', 'Completa la información solicitada', 'Volver');
    }
  }

}
