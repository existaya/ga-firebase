# GA Firebase

En este proyecto encontrarás la implementación del etiquetado de APP híbridas desarrolladas bajo la tecnología de IONIC 4+ para registrar datos en Google Firebase

## Installation

```bash
npm i
```

## Plataforma

```bash
android
```

## IONIC versión
```bash
V 6.3.0
```

## Ng Versión
```bash
     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 8.3.25
Node: 10.15.3
OS: darwin x64
Angular: 8.2.14
... common, compiler, compiler-cli, core, forms
... language-service, platform-browser, platform-browser-dynamic
... router

Package                           Version
-----------------------------------------------------------
@angular-devkit/architect         0.901.0
@angular-devkit/build-angular     0.803.25
@angular-devkit/build-optimizer   0.803.25
@angular-devkit/build-webpack     0.803.25
@angular-devkit/core              9.1.0
@angular-devkit/schematics        9.1.0
@angular/cli                      8.3.25
@ngtools/webpack                  8.3.25
@schematics/angular               9.1.0
@schematics/update                0.803.25
rxjs                              6.5.4
typescript                        3.4.5
webpack                           4.39.2
```

## Dependencias compilación

```javascript
"@angular-devkit/architect": "^0.901.0",
"@angular/common": "~8.2.14",
"@angular/core": "~8.2.14",
"@angular/forms": "~8.2.14",
"@angular/platform-browser": "~8.2.14",
"@angular/platform-browser-dynamic": "~8.2.14",
"@angular/router": "~8.2.14",
"@ionic-native/core": "^5.0.0",
"@ionic-native/firebase-analytics": "^5.23.0",
"@ionic-native/firebase-authentication": "^5.23.0",
"@ionic-native/google-plus": "^5.23.0",
"@ionic-native/splash-screen": "^5.0.0",
"@ionic-native/status-bar": "^5.0.0",
"@ionic/angular": "^5.0.0",
"cordova-android": "^8.1.0",
"cordova-plugin-device": "^2.0.2",
"cordova-plugin-firebase-analytics": "^4.1.2",
"cordova-plugin-firebase-authentication": "^3.1.0",
"cordova-plugin-googleplus": "^8.4.0",
"cordova-plugin-ionic-keyboard": "^2.2.0",
"cordova-plugin-ionic-webview": "^4.1.3",
"cordova-plugin-splashscreen": "^5.0.2",
"cordova-plugin-statusbar": "^2.4.2",
"cordova-plugin-whitelist": "^1.3.3",
"cordova-support-android-plugin": "^1.0.2",
"cordova-support-google-services": "^1.4.0",
"core-js": "^2.5.4",
"rxjs": "~6.5.1",
"tslib": "^1.9.0",
"zone.js": "~0.9.1"
```

## Dependencias desarrollo

```javascript
"@angular-devkit/build-angular": "~0.803.20",
"@angular-devkit/core": "^9.1.0",
"@angular-devkit/schematics": "^9.1.0",
"@angular/cli": "~8.3.23",
"@angular/compiler": "~8.2.14",
"@angular/compiler-cli": "~8.2.14",
"@angular/language-service": "~8.2.14",
"@ionic/angular-toolkit": "^2.2.0",
"@schematics/angular": "^9.1.0",
"@types/jasmine": "~3.3.8",
"@types/jasminewd2": "~2.0.3",
"@types/node": "~8.9.4",
"codelyzer": "^5.0.0",
"jasmine-core": "~3.4.0",
"jasmine-spec-reporter": "~4.2.1",
"karma": "~4.1.0",
"karma-chrome-launcher": "~2.2.0",
"karma-coverage-istanbul-reporter": "~2.0.1",
"karma-jasmine": "~2.0.1",
"karma-jasmine-html-reporter": "^1.4.0",
"npm-force-resolutions": "0.0.3",
"protractor": "~5.4.0",
"ts-node": "~7.0.0",
"tslint": "~5.15.0",
"typescript": "~3.4.3"
```

## Plugins determinantes Ionic Cordova

```javascript
cordova-plugin-firebase-analytics
cordova-plugin-firebase-authentication
```

## License
[MIT](https://choosealicense.com/licenses/mit/)